from datetime import timedelta

from django import forms
from django.forms import ValidationError, TextInput, NumberInput, EmailInput, DateInput, Textarea, FileInput
from django.conf import settings

from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from .models import Account
from django.core.validators import validate_email


class UserCacheMixin:
    user_cache = None


class SignIn(UserCacheMixin, forms.Form):
    password = forms.CharField(label=_('Password'), strip=False, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if settings.USE_REMEMBER_ME:
            self.fields['remember_me'] = forms.BooleanField(label=_('  Remember me'), required=False)

    def clean_password(self):
        password = self.cleaned_data['password']

        if not self.user_cache:
            return password

        if not self.user_cache.check_password(password):
            raise ValidationError(_('Wrong password.'))

        return password


class SignInViaEmailForm(SignIn):
    email = forms.EmailField(label=_('Email'))

    @property
    def field_order(self):
        if settings.USE_REMEMBER_ME:
            return ['email', 'password', 'remember_me']
        return ['email', 'password']

    def clean_email(self):
        email = self.cleaned_data['email']

        user = Account.objects.filter(email__iexact=email).first()
        if not user:
            raise ValidationError(_('Вы ввели неверный email адрес.'))

        if not user.is_active:
            raise ValidationError(_('Данный аккаунт еще не активирован.'))

        self.user_cache = user

        return email


class SignUpForm(UserCreationForm):
    class Meta:
        model = Account
        fields = settings.SIGN_UP_FIELDS

    email = forms.EmailField(label=_('Email'), help_text=_('required email field. Input exist email please.'))
    invite = forms.CharField(label=_('Инвайт'), max_length=50, required=True, help_text='Optional.')

    def clean_email(self):
        email = self.cleaned_data['email']

        user = Account.objects.filter(email__iexact=email).exists()
        if user:
            raise ValidationError(_('Yout can\'t use this email. Email exist.'))

        return email

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.help_text = ''


class ResendActivationCodeForm(UserCacheMixin, forms.Form):
    email_or_username = forms.CharField(label=_('Email or Username'))

    def clean_email_or_username(self):
        email_or_username = self.cleaned_data['email_or_username']

        user = Account.objects.filter(Q(username=email_or_username) | Q(email__iexact=email_or_username)).first()
        if not user:
            raise ValidationError(_('Вы ввели неправильный email/имя ползователя.'))

        if user.is_active:
            raise ValidationError(_('Данный акаунт уже был активирован.'))

        activation = user.activation_set.first()
        if not activation:
            raise ValidationError(_('Код активации не найден.'))

        now_with_shift = timezone.now() - timedelta(hours=24)
        if activation.created_at > now_with_shift:
            raise ValidationError(_('Код активации уже был выслан на ваш почтовый ящик. Код действует 24 часа.'))

        self.user_cache = user

        return email_or_username


class ResendActivationCodeViaEmailForm(UserCacheMixin, forms.Form):
    email = forms.EmailField(label=_('Email'))

    def clean_email(self):
        email = self.cleaned_data['email']

        user = Account.objects.filter(email__iexact=email).first()
        if not user:
            raise ValidationError(_('Вы ввели неправильный email.'))

        if user.is_active:
            raise ValidationError(_('Данный акаунт уже был активирован.'))

        activation = user.activation_set.first()
        if not activation:
            raise ValidationError(_('Код активации не найден.'))

        now_with_shift = timezone.now() - timedelta(hours=24)
        if activation.created_at > now_with_shift:
            raise ValidationError(_('Код активации уже был выслан на ваш почтовый ящик. Код действует 24 часа.'))

        self.user_cache = user

        return email


class RestorePasswordForm(UserCacheMixin, forms.Form):
    email = forms.EmailField(label=_('Email'))

    def clean_email(self):
        email = self.cleaned_data['email']

        user = Account.objects.filter(email__iexact=email).first()
        if not user:
            raise ValidationError(_('Вы ввели неправильный email.'))

        if not user.is_active:
            raise ValidationError(_('This account is not active.'))

        self.user_cache = user

        return email


class RestorePasswordViaEmailOrUsernameForm(UserCacheMixin, forms.Form):
    email_or_username = forms.CharField(label=_('Email or Username'))

    def clean_email_or_username(self):
        email_or_username = self.cleaned_data['email_or_username']

        user = Account.objects.filter(Q(username=email_or_username) | Q(email__iexact=email_or_username)).first()
        if not user:
            raise ValidationError(_('You entered an invalid email address or username.'))

        if not user.is_active:
            raise ValidationError(_('This account is not active.'))

        self.user_cache = user

        return email_or_username


class ChangeProfileForm(forms.Form):
    first_name = forms.CharField(label=_('First name'), max_length=30, required=False)
    last_name = forms.CharField(label=_('Last name'), max_length=150, required=False)


class ChangeEmailForm(forms.Form):
    email = forms.EmailField(label=_('Email'))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data['email']

        if email == self.user.email:
            raise ValidationError(_('Пожалуйста введите другой email.'))

        user = Account.objects.filter(Q(email__iexact=email) & ~Q(id=self.user.id)).exists()
        if user:
            raise ValidationError(_('Вы неможете использовать такой email.'))

        return email


class RemindUsernameForm(UserCacheMixin, forms.Form):
    email = forms.EmailField(label=_('Email'))

    def clean_email(self):
        email = self.cleaned_data['email']

        user = Account.objects.filter(email__iexact=email).first()
        if not user:
            raise ValidationError(_('Вы ввели неверный email address.'))

        if not user.is_active:
            raise ValidationError(_('Данный акаунт еще не активирован.'))

        self.user_cache = user

        return email


class ProfileForm(forms.ModelForm):


    class Meta:
        model = Account
        fields = ['first_name', 'last_name', 'email', 'phone', 'telegram', 'avatar', 'about', 'occupation', 'skills']

        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control'}),
            'second_name': TextInput(attrs={'class': 'form-control'}),
            'last_name': TextInput(attrs={'class': 'form-control'}),
            'email': EmailInput(attrs={'class': 'form-control'}),
            'occupation': TextInput(attrs={'class': 'form-control'}),
            'phone': NumberInput(attrs={'class': 'form-control'}),
            'telegram': TextInput(attrs={'class': 'form-control'}),
            'about': Textarea(attrs={'class': 'form-control'}),
            'skills': Textarea(attrs={'class': 'form-control skills-field'}),
        }


class CreateForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    repeat_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Account
        fields = ['first_name', 'second_name', 'last_name', 'email', 'phone', 'avatar',  'about']

        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control'}),
            'second_name': TextInput(attrs={'class': 'form-control'}),
            'last_name': TextInput(attrs={'class': 'form-control'}),
            'email': EmailInput(attrs={'class': 'form-control'}),
            'phone': NumberInput(attrs={'class': 'form-control'}),
            'avatar': FileInput(attrs={'class': 'form-control'}),
            'about': Textarea(attrs={'class': 'form-control', 'placeholder': "Заметки и описания о клиенте"}),
        }

    def clean(self):
        cleaned_data = super(CreateForm, self).clean()
        password = cleaned_data.get("password")
        repeat_password = cleaned_data.get("repeat_password")

        if password != repeat_password:
            raise forms.ValidationError(
                "Пароли в полях не совпадают или используются очень слабые."
            )


class UpdateUserForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = [
            'first_name',
            'second_name',
            'last_name',
            'email',
            'phone',
            'avatar',
            'about',
            'is_active'
        ]

        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control'}),
            'second_name': TextInput(attrs={'class': 'form-control'}),
            'last_name': TextInput(attrs={'class': 'form-control'}),
            'email': EmailInput(attrs={'class': 'form-control'}),
            'phone': NumberInput(attrs={'class': 'form-control'}),
            'birthday': DateInput(attrs={'class': 'form-control'}),
            'avatar': FileInput(attrs={'class': 'form-control'}),
            'about': Textarea(attrs={'class': 'form-control', 'placeholder': "Заметки и описания о клиенте"}),
        }

