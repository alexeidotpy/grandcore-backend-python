from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _


def index_page(request):
    return render(request, 'index.html', {
        'foo': 'bar',
    }, content_type='text/html')


def about_page(request):
    template = loader.get_template('about.html')
    context = {'text': _('HELLO')}
    return HttpResponse(template.render(context, request))


def home_page(request):
    template = loader.get_template('home.html')
    context = {'go': _('HOME')}
    return HttpResponse(template.render(context, request))


def projects_list(request):
    template = loader.get_template('projects_list.html')
    context = {'project': _('LIFE')}
    return HttpResponse(template.render(context, request))

