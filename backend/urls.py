from django.contrib import admin
from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import index_page, about_page, home_page, projects_list


from django.conf.urls.i18n import i18n_patterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include("api.urls")),
    path('accounts/', include("accounts.urls")),
    path('about/', about_page, name="about"),
    path('', home_page, name="home"),
    path('projects/', projects_list, name="projects-list"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

args = tuple(urlpatterns)
urlpatterns = i18n_patterns(*args)
