from django import forms
from .models import ProjectCategory, Project

class customProjectCategoryUserForm(forms.ModelForm):
    
    class Meta:
        model = ProjectCategory
        fields = ['name', 'parent']

class customProjectUserForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = ['title', 'description_brief', 'description_full', 'category', 'amount_needed', 'amount_available', 'author', 'image', 'file']
