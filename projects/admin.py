from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import Project, ProjectCategory

class ProjectAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'title', 'description_brief', 'amount_needed', 'amount_available',
                ),
            }),
        ('Advanced options', {
            'classes': ('collapse', ),
            'fields': ('description_full', ),
            }),
        ('Extra', {
            'classes': ('extra pretty', ),
            'fields': ('category', ),
            }),
    )
    search_fields = ('title', )
    list_display = ('title', 'category', 'author', 'created')
    list_filter = ('category', ) 


class ProjectCategoryAdmin(MPTTModelAdmin):
    # specify pixel amount for this ModelAdmin only:
    mptt_level_indent = 20


admin.site.register(ProjectCategory, ProjectCategoryAdmin)
admin.site.register(Project, ProjectAdmin)

