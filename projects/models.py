from django.db import models
from django.urls import reverse

from core.models import TimeStampedModel
from accounts.models import Account
from mptt.models import MPTTModel, TreeForeignKey
from crum import get_current_user
from django.utils.translation import ugettext_lazy as _


class ProjectCategory(MPTTModel):
    name = models.CharField(
        max_length=50,
        unique=True,
        verbose_name=_('название категории'))
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children',
        verbose_name=_('родительская категория'))

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = _('категория')
        verbose_name_plural = _('категории проекта')

    def __str__(self):
        return self.name


class Project(TimeStampedModel):
    title = models.CharField(
        max_length=100,
        verbose_name=_('название проекта'))
    description_brief = models.CharField(
        max_length=256,
        blank=True,
        default='',
        verbose_name=_('краткое описание'))
    description_full = models.TextField(
        blank=True,
        default='',
        verbose_name=_('история проекта'))
    category = models.ForeignKey(
        ProjectCategory,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_('категория'))
    amount_needed = models.PositiveIntegerField(
        blank=True,
        null=True,
        default=None,
        verbose_name=_('необходимая сумма'))
    amount_available = models.PositiveIntegerField(
        blank=True,
        null=True,
        default=None,
        verbose_name=_('сумма в наличии'))
    author = models.ForeignKey(
        Account,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
        verbose_name=_('автор проекта'))
    image = models.ImageField(
        upload_to='projects/uploads',
        blank=True,
        verbose_name=_('лого'))
    file = models.FileField(
        upload_to='projects/uploads',
        blank=True,
        verbose_name=_('документы'))

    class Meta:
        verbose_name = _('проект')
        verbose_name_plural = _('проекты')

    def __str__(self):
        return self.title

    def absolute_url(self):
        return reverse("project", kwargs={"id": self.id})

    def save(self, *args, **kwargs):
        user = get_current_user()
        if user and not user.pk:
            user = None
        if not self.pk:
            self.author = user
        self.modified_by = user
        super(Project, self).save(*args, **kwargs)
