from rest_framework.serializers import ModelSerializer
from projects.models import Project, ProjectCategory


class ProjectSerializer(ModelSerializer):

    class Meta:
        model = Project
        fields = "__all__"


class ProjectCategorySerializer(ModelSerializer):

    class Meta:
        model = ProjectCategory
        fields = "__all__"
