from rest_framework.serializers import ModelSerializer
from accounts.models import Account
from rest_framework import serializers
from .invites import InviteSerializer

from taggit_serializer.serializers import (TagListSerializerField, TaggitSerializer)


class AccountSerializer(TaggitSerializer, ModelSerializer):
    skills = TagListSerializerField()

    class Meta:
        model = Account
        fields = "__all__"


class AccountRegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()
    invite = InviteSerializer()
