from django.contrib.auth.mixins import LoginRequiredMixin
from projects.models import Project
from api.serializers.projects import ProjectSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 5


class ProjectViewSet(LoginRequiredMixin, ModelViewSet):
    serializer_class = ProjectSerializer
    queryset = Project.objects.all()
    pagination_class = LargeResultsSetPagination
