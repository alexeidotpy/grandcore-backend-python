from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-
    updating ``created`` and ``modified`` fields.
    """
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('дата создания'))
    modified = models.DateTimeField(
        auto_now=True,
        verbose_name=_('дата последней правки'))

    class Meta:
        abstract = True
