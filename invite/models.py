from django.db import models
import uuid
from accounts.models import Account
from django.utils.translation import ugettext_lazy as _


class Invite(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4)
    registration = models.PositiveSmallIntegerField(
        verbose_name=_('количество регистраций'),
        blank=True,
        null=True
    )
    capacity = models.PositiveSmallIntegerField(
        verbose_name=_('объем регистраций'),
    )
    remaining_volume = models.PositiveSmallIntegerField(
        verbose_name=_('остаток регистраций'),
        blank=True,
        null=True
    )
    created = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return str(self.uuid) or ''

    class Meta:
        verbose_name = _('инвайт')
        verbose_name_plural = _('инвайты')


class InviteRegistration(models.Model):
    invite = models.ForeignKey(
        Invite,
        on_delete=models.CASCADE,
        verbose_name=_('инвайт регистрации')
    )
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        verbose_name=_('пользователь')
    )

    created = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return f'{self.invite} {self.account.email}' or ''

    class Meta:
        verbose_name = _('регистрация')
        verbose_name_plural = _('регистрации')